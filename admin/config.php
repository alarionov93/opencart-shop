<?php
// HTTP
define('HTTP_SERVER', 'http://xn--e1aa6aptdy.xn--p1ai:2222/admin/');
define('HTTP_CATALOG', 'http://xn--e1aa6aptdy.xn--p1ai:2222/');

// HTTPS
define('HTTPS_SERVER', 'http://xn--e1aa6aptdy.xn--p1ai:2222/admin/');
define('HTTPS_CATALOG', 'http://xn--e1aa6aptdy.xn--p1ai:2222/');

// DIR
define('DIR_APPLICATION', '/var/www/xn--e1aa6aptdy.xn--p1ai/admin/');
define('DIR_SYSTEM', '/var/www/xn--e1aa6aptdy.xn--p1ai/system/');
define('DIR_LANGUAGE', '/var/www/xn--e1aa6aptdy.xn--p1ai/admin/language/');
define('DIR_TEMPLATE', '/var/www/xn--e1aa6aptdy.xn--p1ai/admin/view/template/');
define('DIR_CONFIG', '/var/www/xn--e1aa6aptdy.xn--p1ai/system/config/');
define('DIR_IMAGE', '/var/www/xn--e1aa6aptdy.xn--p1ai/image/');
define('DIR_CACHE', '/var/www/xn--e1aa6aptdy.xn--p1ai/system/cache/');
define('DIR_DOWNLOAD', '/var/www/xn--e1aa6aptdy.xn--p1ai/system/download/');
define('DIR_UPLOAD', '/var/www/xn--e1aa6aptdy.xn--p1ai/system/upload/');
define('DIR_LOGS', '/var/www/xn--e1aa6aptdy.xn--p1ai/system/logs/');
define('DIR_MODIFICATION', '/var/www/xn--e1aa6aptdy.xn--p1ai/system/modification/');
define('DIR_CATALOG', '/var/www/xn--e1aa6aptdy.xn--p1ai/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'afhyjcjdf123');
define('DB_DATABASE', 'opencart_test');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
